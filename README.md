Furaffinity downloader

Requirements: python 3

Setup: pip3 install -r requirements.txt

usage: fdl.py [-h] -u USER [-p PATH] [-a API] [-g] [-s] [-f] [-w] [-m]

This is a script that gets all tha artists that the user follows and uses the
scdc script to download all of their uploads

optional arguments:

  -h, --help  show this help message and exit

  -u USER     Set the username

  -p PATH     Set the path to download

  -a API      Use a different FAExport instance

  -g          Option to download gallery

  -s          Option to download scraps
  
  -f          Option to download favorites

  -w          Option to run this script for each person the user is watching

  -m          Option to save metadata for each submission as a json file
  
  TODO: move script to sqlite3