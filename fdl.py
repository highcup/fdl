#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter
import json
import argparse
import os
from pathlib import Path
import logging
from tinydb import TinyDB, Query
import glob
import random
import time
from fake_useragent import UserAgent
from ratelimit import limits, RateLimitException
from backoff import on_exception, expo

# #change retry details for requests
# s = requests.Session()
# retries = Retry(total=5,
#                 backoff_factor=0.5,
#                 status_forcelist=[ 500, 502, 503, 504, 524 ])
# s.mount('https://', HTTPAdapter(max_retries=retries))

#create fake useragent variable
ua = UserAgent(fallback='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36')
#select random useragent via real world browser usage statistic
ua.random

#set logging config
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s:%(levelname)s:%(message)s"
    )

#set default values for global variables
gallery = scraps = watching = favorites = meta = False
user = ''
path = str(Path().absolute())
api_url = 'https://faexport.spangle.org.uk'

@on_exception(expo, RateLimitException, max_tries=8)
#@limits(calls=60, period=1)

#takes user inputs from commandline and saves them to variables
def parse ():
    global gallery, scraps, user, watching, favorites, meta, path, api_url
    logging.debug('parsing arguments')
    #creates the parser with a description
    parser = argparse.ArgumentParser(description='This is a script that gets all tha artists that the user follows and uses the scdc script to download all of their uploads')
	#adds 5 possible arguments
    parser.add_argument('-u', action = "store", dest = 'user', required = True, help = 'Set the username')
    parser.add_argument('-p', action = "store", dest = 'path', required = False, help = 'Set the path to download')
    parser.add_argument('-a', action = "store", dest = 'api', required = False, help = 'Use a different FAExport instance')
    parser.add_argument('-g', action = 'store_true', dest = 'gallery', required = False, help = 'Option to download gallery')
    parser.add_argument('-s', action = 'store_true', dest = 'scraps', required = False, help = 'Option to download scraps')
    parser.add_argument('-f', action = 'store_true', dest = 'favorites', required = False, help = 'Option to download favorites')
    parser.add_argument('-w', action = 'store_true', dest = 'watching', required = False, help = 'Option to run this script for each person the user is watching')
    parser.add_argument('-m', action = 'store_true', dest = 'meta', required = False, help = 'Option to save metadata for each submission as a json file')
    parse = parser.parse_args()

	#sets the global variables: userlink and removedArtist
    user = parse.user
    gallery = parse.gallery
    scraps = parse.scraps
    watching = parse.watching
    favorites = parse.favorites
    meta = parse.meta
    #override default path if given
    if parse.path is not None:
        path = parse.path
    #override default api_url if given
    if parse.api is not None:
        api_url = parse.api
    parse = parser.parse_args()

#@limits(calls=120, period=1)
@on_exception(expo, RateLimitException, max_tries=5)
def requests_retry_session(
    retries=5,
    backoff_factor=0.5,
    status_forcelist=(500, 502, 503, 504, 524),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session

#get profile details about the user
def getUserInfo (username, category=None, pageNumber=0):
    logging.info('running getUserInfo')
    #set default url to the user profile
    url = api_url + '/user/' + username + '.json?'
    #add category to the url if avalible
    if category is not None:
        url = url[:-6] + '/' + category + url[-6:]
    #add page if avalible
    if pageNumber > 1 and category is not 'favorites':
        url += '&page=' + str(pageNumber)
    #add favorites specific pagination
    elif pageNumber > 1 and category is 'favorites':
        url += '&next=' + str(pageNumber)
    #add full=1 if favorites
    if category is 'favorites':
        url += '&full=1'
    #get data
    try:
        #logging.debug('sending get request to: ' + url)
        detail = requests_retry_session().get (url)
    except Exception as e:
        logging.error(e)
        return []
    if detail.status_code != 200:
        logging.info('Server returned non 200 status code. Skip and sleep 30s')
        time.sleep(30)
        return []
    return detail.json()

#get details about a submission
def getSubmissionInfo (id, comments=False):
    logging.info('running getSubmissionInfo')
    #set default url to the submission
    url = api_url + '/submission/' + id + '.json'
    #go to comments if comments is true
    if comments:
        url = url[:-5] + '/comments' + url[-5:]
    #get data
    try:
        logging.debug('sending get request to: ' + url)
        detail = requests_retry_session().get (url)
    except Exception as e:
        logging.error(e)
        return ''
    if detail.status_code == 404:
        logging.info('submission id: ' + id + ' metadata does not exist. skipping')
        return ''
    elif detail.status_code == 429:
        logging.info('submission id: ' + id + ' skipping as the server asks us to slow down. Sleep 30s then skip')
        time.sleep(30)
        return ''
    elif detail.status_code >= 500:
        logging.info('faexport server returned a 500 error: Sleep 30s then skip')
        time.sleep(30)
        return ''
    elif detail.json() is None:
        return []
    return detail.json()

#aggregate all of the user's ids in a category
def aggregateUserInfo (username, category):
    logging.info('running aggregateUserInfo')
    length = page = 1
    info = []
    #loop until nothing is returned
    while length > 0:
        #get the info from the user at page number page
        userinfo = getUserInfo(username, category, page)
        #update the while loops conditional
        length = len(userinfo)
        #loop through the returned info
        for x in userinfo:
            #if category is favorites get submission id from the id key
            if category is 'favorites':
                info.append(x['id'])
            else:
                info.append(x)
        #favorites specific checks
        if category is 'favorites':
            #check to see the last favorites page has been reached by comparing the previous page id to the current generated page id
            if page == int(userinfo[-1]['fav_id']):
                break
            #increment page number by setting page number to the fav_id of the last element
            page = int(userinfo[-1]['fav_id'])
        else:
            page += 1
    return info

def updateDB (db, path):
    logging.info('running updateDB')
    #create a list of all the files in the path that are not json
    local_files = [os.path.basename(x) for x in glob.glob(path + '/*[!.json]')]
    #create the query object
    q = Query()
    #get all the submission files names from the database
    db_files = db.all()
    #loop through the submissions from the database
    for x in db_files:
        #check if the submission in the database exists locally
        logging.debug('check if the file ' + x['filename'] + ' exists locally')
        if x['filename'] not in local_files:
            #remove submission from database it does not exist locally
            db.remove(q.id == x['id'])

def dlSubmission (id, path):
    logging.info('running dlSubmission')
    #get json data about the submission
    try:
        logging.debug('sending get request to: ' + api_url + '/submission/' + id + '.json')
        data = requests_retry_session().get (api_url + '/submission/' + id + '.json')
    except Exception as e:
        logging.error(e)
        return ''
    #check if the submission exists
    if data.status_code == 404:
        logging.info('submission id: ' + id + ' metadata does not exist. skipping')
        return ''
    elif data.status_code == 429:
        logging.info('submission id: ' + id + ' skipping as the server asks us to slow down. Sleep 30s')
        time.sleep(30)
        return ''
    elif data.status_code >= 500:
        logging.info('faexport server returned a 500 error: Sleep 30s then skip')
        time.sleep(30)
        return ''
    #get the filename from the link
    filename = data.json()['download'].rsplit('/', 1)[1]
    #try downloading the submission
    try:
        logging.info('downloading: ' + filename)
        r = requests_retry_session().get (data.json()['download'], stream=True)
        #check if the request is successful
        if r.status_code == 200:
            try:
                #write to the file at the path with the filename
                with open(path + '/' + filename, 'wb') as f:
                    #iterate over the responce
                    for chunk in r.iter_content(1024):
                        # filter out keep-alive new chunks
                        if chunk:
                            f.write(chunk)
            #catch when a file is downloaded with an invalid character in the name and give up
            except Exception as e:
                logging.error(e)
                return''
        if meta:
            logging.debug('writing metadata for ' + filename)
            #write the submission metadata into a json file with the same filename
            with open(path + '/' + filename.rsplit('.',1)[0] + '.json', 'w') as file:
                file.write(json.dumps(data.json()))
        logging.debug('done saving: ' + filename)
    except Exception as e:
        logging.error(e)
        return ''
    except:
        logging.error('Look another exception occurred when trying to download submission id: ' + id)
        return ''
    return filename

def dlCategory (username, path, category):
    logging.info('running dlCategory')
    filename=''
    #add a dash at the end of a username is it ends with a dot
    if username[-1] == '.':
        username_path = username + '-'
    else:
        username_path = username
    #get all the submission ids for the user x
    logging.debug("getting submission ids for a " + username +"'s " + category)
    submission = aggregateUserInfo(username, category)
    if len(submission) == 0:
        logging.debug(username + "'s " + category + ' are empty. skipping')
        return
    logging.info(str(len(submission)) + ' submissions to download from ' + username + "'s " + category)
    #update path
    path += '/' + username_path + '/' + category
    #create the folder if it does not exist
    logging.debug('checking if the path exists')
    if not os.path.exists(path):
        os.makedirs(path)
    #connect to database
    db = TinyDB(path + '/' + username + '_' + category + '.json')
    #create the query object
    q = Query()
    #update database based on what files are already downloaded
    updateDB(db, path)
    logging.debug('loop through submission id to download')
    for s in submission:
        #check if the submission has been downloaded
        if len(db.search(q.id == str(s))) == 0:
            #download the submission and get the filename
            filename = dlSubmission(s, path)
            #try to insert submission into database if downloaded
            if filename != '':
                db.insert({'id': s, 'filename': filename})
        else:
            logging.debug('submission ' + s + ' already downloaded')

def dlUser(username, path):
    logging.info('running dlUser')
    #download the user's gallery if true
    if gallery:
        logging.debug('downloading ' + username + "'s gallery")
        dlCategory(username, path, 'gallery')
    #download the user's scraps if true
    if scraps:
        logging.debug('downloading ' + username + "'s scraps")
        dlCategory(username, path, 'scraps')
    #download the user's favorites if true
    if favorites:
        logging.debug('downloading ' + username + "'s favorites")
        dlCategory(username, path, 'favorites')

def dlWatching (username, path):
    logging.info('running dlWatching')
    #get the usernames of all the people the user is following
    logging.debug('getting the usernames of all the people the user is watching')
    usernames = aggregateUserInfo(username, 'watching')
    #shuffle the usernames
    random.shuffle(usernames)
    #loop through the list of users
    logging.debug('looping through usernames to process')
    for x in usernames:
        #download the user's submissions
        dlUser(x.replace('_',''), path)

#parse command line arguments
parse()
if watching:
    logging.info('starting watch download for the user: ' + user)
    dlWatching(user, path + '/watching')
else :
    logging.info('starting user download for the user' + user)
    dlUser(user, path)